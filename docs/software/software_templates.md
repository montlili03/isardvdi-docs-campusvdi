# Programas en las plantillas

En este apartado se detallarán los programas dentro de cada una de las plantillas disponible.

- **T3_Varios_v6:**

    * **Odoo:** Gestor de base de datos de empresas.
    * **PigUp:** Programa para la creación de granjas.
    * **Landscape:** Programa que diseña paisajes.
    * **LibreOffice:** Paquete de oficina libre.
    * **Notepad++:** Editor de texto con soporte para varios lenguajes de programación.
    * **PDFSam Basic:** Programa que se utiliza para dividir, fusionar, extraer páginas, rotar y mezclar documentos PDF.
    * **7Zip:** Para abrir archivos con formato .zip
    * **Apertium:** Traductor offline
    * **Flameshot:** Herramienta para hacer capturas de pantalla.
    * **4kVideo Downloader:** Herramienta para descargar vídeos de youtube.
    * **GIMP:** Programa de edición de imágenes.
    * **Inkscape:** Editor de gráficos vectoriales libre. Se pueden editar digaramas, líneas, gráficos, logotipos e iliustraciones.
    * **Geany:** Editor de texto.
    * **Zoomit:** Herramienta de zoom, anotación y grabación de pantalla.
    * **Bulk Rename Utility:** Programa diseñado para realizar renombrados masivos de archivos y carpetas. 
    * **Lightshot:** Herramienta para hacer capturas de pantalla.
    * **Chrome, Firefox:** Navegadores de internet
    * **LibreCAD:** Programa libre para diseño 2D.
    * **Node.js:** Programa para crear sitios web escritos con lenguaje de programación JavaScript.
    * **Python 3.10:** Lenguaje de programación


- **T3_Informática_v6:**

    * **Visual Studio code:** Editor de código.
    * **Eclipse IDE:** Editor de código enfocado en el lenguaje Java y en C.
    * **FileZilla Client:** Programa que funciona a nivel cliente/servidor. 
    * **Bluefish:** Editor HTML multiplataforma POSIX y con licencia GPL.
    * **Develop 5:** Programa para la creación de juegos.
    * **PSeint:** Plataforma dirigida a personas que se inician en la programación.​
    * **Apache NetBeans IDE 13:** Programa que permite crear aplicaciones móviles, web y de escritorio usando Java, JavaScript, PHP, HTML5, CSS.
    * **Processing:** Es un lenguaje de programación basado en Java, que sirve como medio para la enseñanza y producción de proyectos multimedia e interactivos de diseño digital.
    * **Android Studio:** Programa para el desarrollo de aplicaciones para Android. 
    * **Brackets:** Editor de código fuente.
    * **LibreCAD:** Programa libre para diseño 2D.
    * **VSCodium:** Editor de código libre.

- **T3_Multimedia_v6:**

    * **Kinovea**: Programa de análisis de vídeo de código abierto enfocado a la actividad deportiva.
    * **Audacity**: Programa de código libre que se puede usar para grabación y edición de audio.
    * **VLC Media Player**: Reproductor y framework multimedia, libre y de código abierto.
    * **OBS Studio**: Aplicación libre y de código abierto para la grabación y transmisión de vídeo por internet.
    * **Shotcut**: Programa de edición de vídeo multiplataforma gratuita y de código abierto.
    * **OpenShot Video Editor**: Programa de edición de vídeo.
    * **4K Slideshow Maker**: Creador de diapositivas con música, diferentes efectos y plantillas.
    * **Scribus 1.4.8**: Programa de maquetación de páginas, creado para el diseño de publicaciones, composición tipográfica,..
    * **Krita**: Programa de pintura digital e ilustración.
    * **MuseScore 3**: Programa de notación musical para la creación y reproducción de partituras.
    * **MiniTool MovieMaker**: Editor de vídeo con el que se puede crear, editar y exportar vídeos.
    * **HandBrake**: Programa para comprimir y preparar vídeos MP4 para poder subirlos a internet.
    * **Affinity Designer**: Editor de gráficos vectoriales para la creación de iconos, logotipos, maquetas y mucho más.
    * **Affinity Photo**: Editor de fotografía, publicación, diseño gráfico e ilustración.
    * **Affinity Publisher**: Programa de maquetación que permite crear, publicar y compartir publicaciones electrónicas o para imprimir.
    * **Wondershare PDF Element**: Programa de edición de PDF.
    * **Wondershare Filmora 11**: Editor de vídeo potente y muy completo.
    * **Wondershare UniConverter 14**: Programa para comprimir archivos MP4.
    * **Kdenlive**: Editor de vídeo con línea de tiempo y múltiples efectos y pistas de audio.

- **T3_Industrial_v6:**

    * **Arduino**: 
    * **GNS3**: Programa que permite simular topologías de red con imaǵenes de marcas como Cisco, Juniper entre otros.
    * **QElectroTech**: Aplicación para la creación de esquemas eléctricos. 
    * **CaRMetal**: Programa de geometría dinámica que permite realizar construcciones geométricas.
    * **Scilab-6.11**: Programa para realizar cálculos numéricos especialmente destinados a ingenierías.
    * **KiCad**: Programa que facilita el diseño de esquemáticos para circuitos electrónicos.
    * **Logism-win**: Programa para el diseño y simulación de circuitos lógicos digitales.
    * **Wireshark**: Analizador de paquetes de red, una utilidad que captura todo tipo de información que pasa a través de una conexión. 

- **T3_3D_v6:**

    * **Blender**: Programa de diseño y animación 3D.
    * **Sweet Home 3D**: Programa de diseño de interiores en un plano 2D y vista previa en 3D.
    * **Slic3r**: Programa de impresión 3D.
    * **FreeCAD**: Modelador 3D para el diseño de cualquier objeto.
    * **Páginas de modelos 3D en el navegador**: Apertium, Thingiverse, Cults, STLFinder, Myminifactory 
    * **Tinkercad**: Aplicación en línea de diseño e impresión en 3D 
    * **Ultimaker Cura**: Aplicación diseñada para impresoras 3D, en la que se pueden modificar los parámetros de impresión y después transformarlos a código G.

