# Enlaces

En este [enlace](https://nextcloud.isardvdi.com/s/iAgWmGqpMSFQM7H) se encuentran PDFs como:


* **Presentación de Isard**:

[![](material_classes.images/isard.png)](https://nextcloud.isardvdi.com/s/xoizkQfbKnrwYkD)

* **Pequeña explicación sobre los visores**:

[![](material_classes.images/visores.png)](https://nextcloud.isardvdi.com/s/dZoTZoFW3fHLfM9)