# Introducción

![](./intro/logo_aragon.png)

**Servicio de escritorios virtuales para centros públicos de Formación Profesional de Aragón**

El servicio de escritorios virtuales basado en el programario [IsardVDI](https://isardvdi.com) es un proyecto que está dando servicio a centros educativos de formación profesional en la comunidad autónoma de Aragón. 

El proyecto se encuentra enmarcado dentro de la iniciativa de conversión de espacios en aulas de tecnología aplicada (ATECA), donde los centros elegidos han colaborado para dotar a esta infraestructura de hardware y software profesional que pueda contribuir a la transformación tecnológica de la FP de manera transversal, impactando en todas las familias profesionales y no solo en las de carácter más tecnológico.

Esta iniciativa está impulsada por el Ministerio de Educación y Formación Profesional y financiada por la Unión Europea (fondos NextGenerationEU) e implantada en Aragón a través de la Dirección General de Innovación y Formación Profesional, actualmente a través del centro de formación profesional Campus Digital.

De momento está disponible para 18 centros educativos así como para centros de formación (CATEDU) y de innovación (CIFPA)  y se irá ampliando de manera progresiva según crezca la iniciativa y se pueda abrir a más centros. Si hay alguna duda sobre si un centro está dado de alta en este servicio, o quiere participar, se puede enviar un correo a <soporte-cifpa@isardvdi.com> (si es una cuestión técnica de la plataforma) o a <campusdigital@aragon.es> si la duda refiere a temas más relacionados con la gestión del servicio desde el Departamento de Educación.

Para el funcionamiento más detallado de Isard se dispone del manual de la herramienta en: [https://isard.gitlab.io/isardvdi-docs](https://isard.gitlab.io/isardvdi-docs/index.es/). Aquí se tiene un listado con acceso directo a los apartados destacados de este manual para el profesorado:

* [Visores](https://isard.gitlab.io/isardvdi-docs/user/viewers.es/): Explicación de los tipos de visores ventajas e inconvenientes       

* [Despliegues](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.es/): Despliegues de escritorios como profesor

* [Plantillas](https://isard.gitlab.io/isardvdi-docs/advanced/templates.es/): Creación de plantillas


## Conceptos básicos de la plataforma

- **Visores:** Es una herramienta que sirve para visualizar un escritorio.

- **Despliegues:** Es una creación masiva de escritorios para distintos grupos y/o usuarios que se puede monitorizar todos los escritorios.

- **Plantillas:** Es una copia de un escritorio virtual que conserva el estado del sistema, esto nos permite crear más escritorios a partir de dicha base.

- **Escritorio virtual:** Son imágenes preconfiguradas de sistemas operativos y aplicaciones en los que el entorno de escritorio está separado del dispositivo físico utilizado para acceder a él. Los usuarios pueden acceder a sus escritorios virtuales de manera remota y con cualquier dispositivo.

- **Visor directo:** Es un enlace para poder visualizar un escritorio sin tener acceso al propio escritorio.

- **Isos:** Imagen binaria de CD/DVD.

- **Medios:** Apartado donde se pueden subir isos (imágenes de discos)

- **Escritorio Persistente:** Es un escritorio que no se borra, cuando se apague, no se perderá la información o programas que se hayan instalado. 

- **Escritorio temporal:** Como su nombre indica es un escritorio "temporal", eso quiere decir que una vez arrancado un escritorio, cuando se cierre se perderá todo lo que se haya guardado o descargado.

- **GPU:** Es un procesador que se dedica al procesamiento de gráficos para aligerar la carga de trabajo de la CPU en aplicaciones como los videojuegos o aplicaciones 3D.

- **Boot:** Es el modo de arranque, desde donde se inicia el sistema. 

- **Cuotas:** Restricción que tiene un usuario al crear escritorios, plantillas, subir media, etc. en el sistema. 


## Para qué sirve y porqué utilizar Isard

Con IsardVDI se pueden crear escritorios remotos y virtuales desde cualquier sitio independientemente del equipo que se tenga. Una de las ventajas es que si se tiene un equipo antiguo, no es necesario ir actualizando las máquinas, ya que todo es virtual y se puede tener el sistema operativo que requiera en cada momento. 

Otra ventaja de tener Isard es que se pueden organizar clases a medida, es decir, se pueden crear escritorios ya con programas específicos para cada asignatura sin tener que perder tiempo ni recursos de los alumnos/equipo informático de los centros.


## Casos de uso 

Estos son los usos más frecuentes de la plataforma de IsardVDI:

- **"En mi centro o en casa no tengo equipo suficiente para correr ciertos programas"** - Los escritorios en Isard tienen bastante capacidad y no habrá problema de RAM ni de CPU.

- **"El alumno pierde mucho tiempo instalando software."** - Para ello hay plantillas dentro de la plataforma que pueden personalizar los profesores con los programas necesarios para dar sus clases.

- **"Tengo un equipo con un sistema operativo diferente al del centro y no son compatibles los programas con mi ordenador."** - Independientemente del sistema operativo que se tenga, se puede utilizar Isard, ya que funciona via web, y si se tiene conexión a internet se puede acceder fácilmente. Dentro de Isard se puede tener cualquier sistema operativo.

- **"No quiero descargarme todos los programas de mi ciclo en mi ordenador"** - Para ello se pueden tener uno o más escritorios con los programas necesarios y así no tener que descargar nada en el ordenador propio.

- **"Quiero hacer una clase de redes con un programa en concreto"** - No hace falta ya tener que pedir servicio técnico a informáticos del centro para que se pueda instalar programas en los ordenadores del centro.

- **"Tengo una práctica que quiero ir haciendo con la clase"** - Para esta función está hecho los "Despliegues". Se puede crear una "clase" con los usuarios que se quiera, de la plantilla que se necesite y guiar a los alumnos por el "videowall" de la instalación.
