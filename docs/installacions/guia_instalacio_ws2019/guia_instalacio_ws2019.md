# Guia d'instal·lació Windows Server 2019

La intenció d'aquesta guia és mostrar pas a pas com crear una imatge base de Windows Server 2019 per crear una plantilla adaptada a l'entorn de virtualització d'escriptoris d'IsardVDI.

La idea d'aquest document és aportar i recollir idees sobre quins elements de Windows modificar i personalitzar per un millor rendiment.


## Pre-instal·lació Windows Server 2019

### Creació de l'escriptori

1.- Crear **nou escriptori** des del panell principal

2.- Aplicar **visors** sel·leccionats, xarxes **Default** i **VPN** i marcar l'**arrencada de l'escriptori des d'imatge ISO**

![](./guia_instalacio_ws2019.images/dWNL9cl.png)


3.- Crear l'escriptori en base a la plantilla **Windows Server 2019 ISO install**

![create_desktop_install](./guia_instalacio_ws2019.images/uJIyLj9.png)


4.- A l'apartat **Media** de l'escriptori s'hi afegiran les imatges que porta la plantilla, com són l'**instal·lador de Windows Server 2019** i els **controladors de VirtIO** amb l'agent d'instal·lació("win2019 eval" i "virtio-win" respectivament)

![](./guia_instalacio_ws2019.images/EHwqTEc.png)
 

5.- Un cop creat, **arrencar l'escriptori**


## Instal·lació

### Sistema Operatiu

1.- Si surt aquesta pantalla a l'arrencada, **enviar "Ctr+Alt+Supr"**

![](./guia_instalacio_ws2019.images/8oqNxy8.png)

![](./guia_instalacio_ws2019.images/JG4kD1Q.png)

![](./guia_instalacio_ws2019.images/dHk0hkw.png)


2.- Escollir **"Experiencia de escritorio"** per **"Standard Evaluation"**

![](./guia_instalacio_ws2019.images/OVUbe6T.png)


3.- Escollir la **instal·lació personalitzada**

![](./guia_instalacio_ws2019.images/h4LGdaT.png)


### Carregar drivers


1.- Ara s'han de **carregar els controladors de VirtIO** perquè el sistema operatiu el reconegui com un **tipus vàlid de disc**. Es fa clic a "Cargar controlador"

![](./guia_instalacio_ws2019.images/CZm4guM.png)


2.- Es busca a l'explorador la **unitat de disc extraíble** corresponent a la imatge **virtio-win** i es sel·lecionna l'arxiu corresponent per a Windows Server 2019

![](./guia_instalacio_ws2019.images/VvDyAED.png)

![](./guia_instalacio_ws2019.images/bm5lrRc.png)

![](./guia_instalacio_ws2019.images/0Nyv2cG.png)

![](./guia_instalacio_ws2019.images/qwgRZDo.png)


3.- **Mostrar** els detalls de l'error de connexió i **acceptar**

![](./guia_instalacio_ws2019.images/9I0BEIi.png)

![](./guia_instalacio_ws2019.images/iSCDywB.png)


4.- Fer clic a **"Siguiente"** i **començarà la instal·lació del sistema operatiu**

![](./guia_instalacio_ws2019.images/oFiNaaf.png)


## Post-instal·lació

### Modificació de l'escriptori

1.- **Canviar l'arrencada** de l'escriptori des de ISO a arrencar amb el **disc dur**

![](./guia_instalacio_ws2019.images/dpBb8Pw.png)

![](./guia_instalacio_ws2019.images/acLq1Ng.png)


2.- S'arrenca l'escriptori


3.- El primer que s'obrirà serà aquest panell on s'ha de canviar el password. En aquest cas a ***Aneto_3404***

![](./guia_instalacio_ws2019.images/YPy7q5M.png)


4.- S'**envia** altre cop la combinació **"Ctrl+Alt+Supr"**

![](./guia_instalacio_ws2019.images/beVwtF5.png)


### Instal·lació de drivers

1.- S'obre altre cop la imatge de **"virtio-win"** i s'executa l'arxiu per **instal·lar l'agent** i els controladors necessaris de VirtIO, com per exemple, els components de xarxa

![](./guia_instalacio_ws2019.images/ipRyw14.png)

![](./guia_instalacio_ws2019.images/Kt5h8a1.png)

![](./guia_instalacio_ws2019.images/9Fhlfg8.png)

![](./guia_instalacio_ws2019.images/jwPcjg4.png)

![](./guia_instalacio_ws2019.images/8dPDa6i.png)

![](./guia_instalacio_ws2019.images/e7PGTOF.png)


2.- Es **reinicia l'equip** i s'instal·len la **'virtio-win-guest-tools'**, on hi ha el **'qemu-guest-agent'**, que permetrà la comunicació del sistema de l'escriptori amb el **visor**, i així pugui reconéixer la **redimensió de pantalla** o la lectura de **dispositius USB** des de la màquina *host*

![](./guia_instalacio_ws2019.images/S9tH3vy.png)

![](./guia_instalacio_ws2019.images/VPJ1kaY.png)


### Escriptori remot

1.- S'habilita l'**escriptori remot** a la **'Configuración'** del sistema

![](./guia_instalacio_ws2019.images/OmAPWKi.png)

![](./guia_instalacio_ws2019.images/zcExQev.png)

![](./guia_instalacio_ws2019.images/tglLC70.png)


2.- A **configuració avançada**:

![](./guia_instalacio_ws2019.images/kK4hnmR.png)

![](./guia_instalacio_ws2019.images/YD6EGoK.png)


### Verificació de xarxes

Es verifiquen el **correcte funcionament** de les **xarxes** configurades amb el comandament ```ipconfig``` obrint un CMD

![](./guia_instalacio_ws2019.images/4IPIky9.png)


### Login per visor RDP VPN

Es modifica l'escriptori i s'**activa el visor "RDP VPN"**, i es **canvien les credencials** per què siguin les **mateixes** que l'usuari i contrassenya que el **login del Windows**.

D'aquesta forma es **connectarà automàticament per RDP sense validació**.

(Seguint els passos de https://learn.microsoft.com/en-us/troubleshoot/windows-server/user-profiles-and-logon/turn-on-automatic-logon)

![](./guia_instalacio_ws2019.images/957mTsW.png)


## Creació de T3

Les **plantilles T3** són les plantilles de **Windows** on s'ha **passat el programari *Optimization Tools***. 
Aquest **treu** els **serveis inutilitzats** que omplen **espai** en disc i memòria i **empeoren** la gestió d'energia. 
**Millora** així el **rendiment** en la totalitat l'escriptori.

Els passos del manual fins aquí expliquen la **creació de la plantilla T1**, on un cop acabada la instal·lació es treuen les imatges de l'apartat **Media** i es converteix en plantilla T1.- 

En aquest últim punt s'executa el software per **crear la T3**.


### Optimization tools

S'executa l'eina **Optimization Tools** tal i com s'[explica a aquest apartat del manual](https://isard.gitlab.io/isardvdi-docs/install/win10_install_guide.ca/#vmware-us-optimization-tool) per crear una plantilla T3, la qual a Isard li diem així després que un escriptori hi passi pel software d'Optimization tools.

1.- Existeix una imatge ja creada que s'ha d'**afegir a la T1** i **crear un escriptori** per poder **executar el programa**, i posteriorment **crear la plantilla nomenada T3**

![](./guia_instalacio_ws2019.images/Hx8YIMy.png)


2.- **Arrencar** l'escriptori i executar el programari

![](./guia_instalacio_ws2019.images/uV9Pgy9.png)


3.- S'obre aquesta finestra i fem clic a **'Analyze'**

![](./guia_instalacio_ws2019.images/zTnYuQV.png)


4.- A **'Opciones comunes'** es marquen aquests **paràmetres**

![](./guia_instalacio_ws2019.images/yE5LsHe.png)

![](./guia_instalacio_ws2019.images/WbZhJFc.png)


5.- Es fa clic a **'Optimizar'** per iniciar l'optimització


6.- Es **reinicia** l'escriptori per comprovar el correcte funcionament i veure que s'ha **millorat la velocitat** del sistema


7.- Finalment s'**apaga** l'escriptori, es modifica i es **treu la imatge de l'apartat "Media"**


8.- Sobre aquest escriptori es **crea una nova plantilla T3**

