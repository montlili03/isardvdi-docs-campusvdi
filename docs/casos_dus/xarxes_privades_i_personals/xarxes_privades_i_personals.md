# Xarxes privades i personals

## Explicació

Una xarxa **privada** es configura a múltiples escriptoris a la vegada per establir una comunicació entre màquines de **múltiples usuaris**. 

*- Exemple de casos d’ús: arquitectura client-servidor entre escriptoris i entre escriptori i màquina client.*

Una xarxa **personal** es configura a múltiples escriptoris a la vegada per establir una comunicació entre màquines del **mateix usuari**. 

*- Exemple de casos d’ús: arquitectura client-servidor on múltiples clients fan servir la mateixa adreça IP.*

Es poden **combinar** ambdós tipus de xarxa en un mateix escriptori i entorn virtual, així com també targetes de **sortida a Internet**. Els escriptoris virtuals d’Isard permeten també funcionar com **routers** o **servidors** que surtin a la xarxa amb la targeta WireGuard (crear una connexió **VPN** personal) i que siguin així accessibles des d’altres escriptoris i/o des de la màquina client.

Es poden **modificar els permisos d’accés** a aquestes xarxes per limitar quins usuaris o grups poden utilitzar-les.

*- Exemple de casos d’ús: arquitectura client-servidor DHCP amb dos escriptoris respectivament fent servir una xarxa privada, permetent utilitzar amb posterioritat una xarxa personal modificant l’escriptori i les seves xarxes.*


## Manual d'usuari

### Rol advanced

#### Escriptori servidor

1.- [**Crear un escriptori**](https://isard.gitlab.io/isardvdi-docs/user/create_desktop.ca/#crear-escriptori) agafant plantilla base de **Debian 11 Desktop** configurada a Isard

![](./xarxes_privades_i_personals.images/0vZ63DU.png)


2.- Configurar xarxes (al moment o [editar-lo posteriorment](https://isard.gitlab.io/isardvdi-docs/user/edit_desktop.ca/#editar-escriptori)), afegint **‘Defualt’** i **‘badia1’** (**adaptador pont** i **privada** respectivament)

![](./xarxes_privades_i_personals.images/e4UOfKp.png)

![](./xarxes_privades_i_personals.images/1V2ayAH.png)


3.- Arrencar l’escriptori i [modificar la **configuració** de xarxa](https://isard.gitlab.io/isardvdi-docs/user/edit_desktop.ca/#hardware) d’ambdues targetes, corresponent la primera a ‘**Default**’ i la segona a ‘**badia1**’ (la segona targeta es configura amb **ip fixa**)

![](./xarxes_privades_i_personals.images/oihGAPD.png)

![](./xarxes_privades_i_personals.images/VratvuJ.png)

![](./xarxes_privades_i_personals.images/7J4O1kL.png)


> NOTA: l’ordre de targetes de xarxa que es configuren en l’escriptori a l’Isard és l’ordre de targetes que apareixen configurades al sistema de l’escriptori, per exemple, quan s’escriu al terminal el comandament ```ip -c a```.


4.- Actualitzar els repositoris i **instal·lar el paquet ‘apache2’**. Verificar el seu funcionament

![](./xarxes_privades_i_personals.images/p9xAC9K.png)


#### Escriptori client

1.- [Crear un escriptori agafant plantilla base](https://isard.gitlab.io/isardvdi-docs/user/create_desktop.ca/#crear-escriptori) d’**Ubuntu 22.04 Desktop** configurada a Isard

![](./xarxes_privades_i_personals.images/pYKsB50.png)


2.- [Configurar xarxes](https://isard.gitlab.io/isardvdi-docs/user/edit_desktop.ca/#hardware), afegint només la privada **‘badia1’**

![](./xarxes_privades_i_personals.images/bRbf6WK.png)


3.- Arrencar l’escriptori i modificar la **configuració** de xarxa

![](./xarxes_privades_i_personals.images/rK3xhII.png)


- **Ping al servidor**:

![](./xarxes_privades_i_personals.images/W3AOtwN.png)


4.- Obrir un **navegador web** i verificar l’accés al **servidor web** mitjançant la seva **adreça IP**

![](./xarxes_privades_i_personals.images/H6rCT5L.png)


5.- **Apagar** escriptoris *adequadament* (des del sistema, gràficament o amb comandaments)


6.- [Convertir els escriptoris a **plantilla**](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#plantilles), [habilitar-les (fer **visible**)](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#fer-visibleinvisible) i [**compartir** amb els usuaris desitjats](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#compartir) 

![](./xarxes_privades_i_personals.images/Ej0xSKI.png)


- Es pot **fer plantilla** també el **client** i així tenir ja la configuració de xarxa adient:

![](./xarxes_privades_i_personals.images/qqO0Q8P.png)


### Rol user

#### Escriptoris a partir de plantilles

Iniciar sessió a Isard i comprovar la visibilitat de les plantilles creades i compartides pel **professor**

![](./xarxes_privades_i_personals.images/TNHdgNf.png)


> NOTA: crear un escriptori, si no es modifica, hereta les configuracions de hardware de la plantilla. Es pot modificar en crear l’escriptori o un cop ja creat amb el botó d’edició.


Es pot [**canviar**](https://isard.gitlab.io/isardvdi-docs/user/edit_desktop.ca/#editar-escriptori), per exemple, la xarxa privada **‘badia1’** per la xarxa **‘personal-badia’** a **servidor** i a **client**, perquè l’alumne pugui realitzar la pràctica en un **segment de xarxa exclusiu**, i poder així cada alumne fer servir les **mateixes adreces IP sense col·lisionar**.

![](./xarxes_privades_i_personals.images/gTwVVEH.png)

![](./xarxes_privades_i_personals.images/dLx9n0z.png)

![](./xarxes_privades_i_personals.images/iN98Rvq.png)

![](./xarxes_privades_i_personals.images/dX5pLqa.png)

![](./xarxes_privades_i_personals.images/FhUtMy6.png)

![](./xarxes_privades_i_personals.images/b3gFOK2.png)